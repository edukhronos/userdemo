package sample;

public class User {
  private int id;
  private String nickname;
  private String pass;
  private String firstName;
  private String lastName;
  private String mail;
  private String date;

  public int getId() {
    return id;
  }
  public String getNickname() {
    return nickname;
  }
  public String getPass() {
    return pass;
  }
  public String getFirstName() {
    return firstName;
  }
  public String getLastName() {
    return lastName;
  }
  public String getMail() {
    return mail;
  }
  public String getDate() {
    return date;
  }

  public void setId(int id) {
    this.id = id;
  }
  public void setNickname(String nickname) {
    this.nickname = nickname;
  }
  public void setPass(String pass) {
    this.pass = pass;
  }
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
  public void setMail(String mail) {
    this.mail = mail;
  }
  public void setDate(String date) {
    this.date = date;
  }
}
