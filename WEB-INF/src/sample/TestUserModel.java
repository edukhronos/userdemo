package sample;

import java.sql.*;
import java.util.*;

public class TestUserModel {
  public static void main(String[] args){
      UserModel model = new UserModel();
      ArrayList<User> users = model.getUsers();

      System.out.println("list num: " + users.size());
      for(User user : users) {
        System.out.println("ID: " + user.getNickname());
      }
  }
}
